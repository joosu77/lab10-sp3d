![Failed to show the logo!](./res/logo-minimal.png){width=128}

# We are the team SP3D:
1. Joosep Näks
2. Semjon Kravtšenko
3. Uku Hannes Arismaa

#### And here is our first homework: [Link](https://bitbucket.org/joosu77/lab10-sp3d/wiki/Homework%201 "Homework 1")
#### [Link to the second homework](https://bitbucket.org/joosu77/lab10-sp3d/wiki/Homework%202 "Homework 2")
#### [Link to the fourth homework](https://bitbucket.org/joosu77/lab10-sp3d/wiki/Homework%204 "Homework 4")
#### [Link to the sixth homework](https://bitbucket.org/joosu77/lab10-sp3d/wiki/Homework%206 "Homework 6")

###### How to use our fantastic software:
```
bash ./gradlew build # -Dorg.gradle.java.home=/JDK_PATH
bash ./gradlew hsql & # This will run the database
bash ./gradlew run
```
Or just download the distribution [for Ubuntu](https://drive.google.com/drive/folders/14vWw6ha20991PYqhPuVIw3tBFIZKkrE5) or [for Microsoft Windows 10](https://drive.google.com/drive/folders/155u6lFKx9XJrDehASec6JsLwe4w-Wvl-), unpack both zip archives, find bin folder and start an appropriate file in there.  


We kindly encourage you to use [markdown syntax](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html) whenever you can!