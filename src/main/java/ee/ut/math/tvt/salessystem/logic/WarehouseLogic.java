package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.util.List;

public class WarehouseLogic {

    private final SalesSystemDAO dao;
    public WarehouseLogic(SalesSystemDAO dao) {
        this.dao = dao;
    }

    public void addProduct(StockItem item) throws SalesSystemException {
        if (item.getName().equals(""))
            throw new SalesSystemException("Name can not be !");
        if (item.getId() < 0 || item.getPrice() < 0)
            throw new SalesSystemException("A negative value is not allowed (Ein negativer Wert ist nicht zulässig)");
        if (dao.findStockItem(item.getId()) == null && item.getQuantity() <= 0)
            throw new SalesSystemException("Negative number of new things. Does not make sense.");
        if (dao.findStockItem(item.getId()) != null && item.getQuantity() < - dao.findStockItem(item.getId()).getQuantity())
            throw new SalesSystemException("In order to throw away something unnecessary, you must first get something unnecessary");
        if (dao.findStockItem(item.getId()) != null && (!dao.findStockItem(item.getId()).getName().equals(item.getName()) || dao.findStockItem(item.getId()).getPrice()!=item.getPrice()))
            throw new SalesSystemException("A product with the id already exists, enter a new id or make sure that the name and price match with the existing item.");
        if (dao.findStockItem(item.getName()) != null && !dao.findStockItem(item.getName()).getId().equals(item.getId()))
            throw new SalesSystemException("A product with the name already exists with an id of "+dao.findStockItem(item.getName()).getId()+", make sure that the id matches.");

        dao.beginTransaction();
        StockItem present = dao.findStockItem(item.getId());
        if (present != null) {
            item.setQuantity(item.getQuantity() + present.getQuantity());
            if (item.getQuantity() != 0) {
                dao.removeStockItem(item.getId());
                dao.addStockItem(item);
            } else {
                dao.removeStockItem(item.getId());
            }
        } else {
            if (item.getQuantity() != 0) {
                dao.addStockItem(item);
            }
        }
        dao.commitTransaction();
    }

    public void removeStockItem(Long id) {
        if (dao.findStockItem(id) == null)
            throw new SalesSystemException("Item with that id does not exist");
        else {
            dao.beginTransaction();
            dao.removeStockItem(id);
            dao.commitTransaction();
        }
    }

    public List<StockItem> findStockItems() {
        return dao.findStockItems();
    }

    public StockItem findStockItem(String text) {
        return dao.findStockItem(text);
    }

    public StockItem findStockItem(Long id) {
        return dao.findStockItem(id);
    }
}
