// Every function emulates 1 SQL action

package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dataobjects.PurchaseItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class InMemorySalesSystemDAO implements SalesSystemDAO {
    // For testing purposes. This will NEVER work with a real database!
    // Holds distinct ints for all called modifying functions
    public final List<Integer> testActionsMade = new ArrayList<>();

    private final List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private final List<PurchaseItem> purchaseItemList;
    private Long purchaseItemId;

    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<>();
        items.add(new StockItem(1L, "Lays chips", "Potato chips", 1100, 5));
        items.add(new StockItem(2L, "Chupa-chups", "Sweets", 80, 8));
        items.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 1500, 12));
        items.add(new StockItem(4L, "Free Beer", "Student's delight", 0, 100));
        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();
        this.purchaseItemList = new ArrayList<>();
        this.purchaseItemId = 0L;
    }

    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public StockItem findStockItem(String name) {
        for (StockItem item : stockItemList) {
            if (item.getName().equals(name))
                return item;
        }
        return null;
    }

    @Override
    public List<SoldItem> findSoldItems() {
        return soldItemList;
    }

    @Override
    public SoldItem findSoldItem(long id) {
        // TODO
        return null;
    }

    @Override
    public List<PurchaseItem> findPurchaseItems() {
        return purchaseItemList;
    }

    @Override
    public PurchaseItem findPurchaseItem(long id) {
        for (PurchaseItem item : purchaseItemList) {
            if (item.getId().equals(id))
                return item;
        }
        return null;
    }

    @Override
    public List<PurchaseItem> findLast10Purchases(){
        if (purchaseItemList.size()>10) {
            purchaseItemList.sort(new Comparator<PurchaseItem>() {
                @Override
                public int compare(PurchaseItem purchaseItem, PurchaseItem t1) {
                    return purchaseItem.getDate().compareTo(t1.getDate());
                }
            });
            return purchaseItemList.subList(purchaseItemList.size() - 11, purchaseItemList.size() - 1);
        } else {
            return purchaseItemList;
        }
    }

    @Override
    public List<PurchaseItem> findPurchasesBetweenDates(LocalDate from, LocalDate to) {
        List<PurchaseItem> between=new ArrayList<>();
        for (PurchaseItem elem:purchaseItemList){
            if(elem.compareTo(from)>=0 && elem.compareTo(to)<=0){
                between.add(elem);
            }
        }
        return between;
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        testActionsMade.add(1);

        soldItemList.add(item);
    }


    @Override
    public void savePurchaseItem(List<SoldItem> items) {
        testActionsMade.add(7);

        purchaseItemId = getPurchaseItemId();
        PurchaseItem item = new PurchaseItem(purchaseItemId++, items);
        purchaseItemList.add(item);
    }

    @Override
    public void addStockItem(StockItem stockItem) {
        testActionsMade.add(8);

        stockItemList.add(stockItem);
    }

    @Override
    public void removeStockItem(Long id) {
        testActionsMade.add(10);

        Optional<StockItem> stockItem = stockItemList.stream().filter(sItem -> sItem.getId().equals(id)).findAny();
        stockItem.ifPresent(stockItemList::remove);
    }

    @Override
    public void beginTransaction() {
        testActionsMade.add(4);
        // Does nothing
    }

    @Override
    public void rollbackTransaction() {
        testActionsMade.add(5);
        // Does nothing
    }

    @Override
    public void commitTransaction() {
        testActionsMade.add(6);
        // Does nothing
    }

    @Override
    public Long getPurchaseItemId(){
        while (findPurchaseItem(purchaseItemId)!=null){
            purchaseItemId++;
        }
        return purchaseItemId;
    }
}
