package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.PurchaseItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HistoryLogic {
    private final SalesSystemDAO dao;
    private final List<PurchaseItem> items = new ArrayList<>();
    public HistoryLogic(SalesSystemDAO dao) {
        this.dao = dao;
    }

    public PurchaseItem findPurchaseItem(Long id) {
        return dao.findPurchaseItem(id);
    }

    public List<PurchaseItem> getAll() {
        return dao.findPurchaseItems();
    }

    public List<PurchaseItem> getLastTen() {
        /*List<PurchaseItem> items2=dao.findPurchaseItems();
        Collections.sort(items2);
        return items2.subList(java.lang.Math.max(0,items2.size()-10),items2.size());*/
        return dao.findLast10Purchases();
    }

    public List<PurchaseItem> getBetweenDates(LocalDate from, LocalDate toInclusive) throws SalesSystemException {
        if (from.isAfter(toInclusive))
            throw new SalesSystemException("Start date > End date;; this is _illegal_!");

        return dao.findPurchasesBetweenDates(from, toInclusive);
    }

}
