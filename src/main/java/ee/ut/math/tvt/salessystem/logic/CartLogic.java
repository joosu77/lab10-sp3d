package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CartLogic {

    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>(); // Items of current purchase
    WarehouseLogic warehouseLogic; // Better make it a singleton

    public CartLogic(SalesSystemDAO dao, WarehouseLogic warehouseLogic) {
        this.dao = dao;
        this.warehouseLogic = warehouseLogic;
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(StockItem stockItem, int quantity) throws SalesSystemException {
        SoldItem itemToAdd = new SoldItem(stockItem,quantity,dao.getPurchaseItemId());
        Optional<SoldItem> soldItem = items.stream().filter(sItem -> sItem.getId().equals(itemToAdd.getId())).findAny();
        if (soldItem.isPresent()) {
            itemToAdd.setQuantity(soldItem.get().getQuantity() + itemToAdd.getQuantity());
        }

        if (itemToAdd.getQuantity() < 0)
            throw new SalesSystemException("Not possible for item to be in negative quantity!");
        if (dao.findStockItem(itemToAdd.getId()).getQuantity() < itemToAdd.getQuantity())
            throw new SalesSystemException("BLACK FRIDAY GONE WRONG! Not enough in stock!");

        if (soldItem.isPresent()) items.remove(soldItem.get());
        if (itemToAdd.getQuantity() == 0) return;

        items.add(itemToAdd);
    }

    public void removeItem(Long idToRemove) {
        Optional<SoldItem> soldItem = items.stream().filter(sItem -> sItem.getId().equals(idToRemove)).findAny();
        if (soldItem.isPresent()) {
            items.remove(soldItem.get());
        } else {
            throw new SalesSystemException("You can not remove what you do not have in cart");
        }
    }

    public List<SoldItem> getAll() {
        return items;
    }

    public void cancelCurrentPurchase() {
        items.clear();
    }

    public void submitCurrentPurchase() {
        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596
        dao.beginTransaction();
        try {
            for (SoldItem item : items) {
                dao.saveSoldItem(item);
                StockItem stockItem = dao.findStockItem(item.getStockItem().getId());
                StockItem availble = warehouseLogic.findStockItem(stockItem.getId());
                availble.setQuantity(availble.getQuantity() - item.getQuantity());

                if (availble.getQuantity() > 0) {
                    dao.removeStockItem(availble.getId());
                    dao.addStockItem(availble);
                }
                else dao.removeStockItem(availble.getId()); // ERADICATE THE EMPTIED AND USELESS ITEM
            }
            dao.savePurchaseItem(new ArrayList<>(items));
            items.clear();
            dao.commitTransaction();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }

    public StockItem findStockItem(String name) {
        return dao.findStockItem(name);
    }
    public StockItem findStockItem(Long id) {
        return dao.findStockItem(id);
    }
    public List<StockItem> findStockItems() {
        return dao.findStockItems();
    }
}
