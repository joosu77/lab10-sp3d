package ee.ut.math.tvt.salessystem.logic;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class PropertyValues {
    public String teamName = null;
    public String leader = null;
    public String email = null;
    public List<String> members = null;
    public List<String> logoascii = null;

    public int getMemnum(){
        return members.size();
    }
    public PropertyValues() {
        Properties prop = new Properties();
        String propFileName = "config.properties";
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
        try {
            InputStreamReader reader = new InputStreamReader(inputStream, "UTF-8");
            prop.load(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }

        teamName = prop.getProperty("team");
        leader = prop.getProperty("leader");
        email = prop.getProperty("email");
        members = Arrays.asList(prop.getProperty("members").split(","));
        logoascii = Arrays.asList(prop.getProperty("logoascii").split(","));
    }
}
