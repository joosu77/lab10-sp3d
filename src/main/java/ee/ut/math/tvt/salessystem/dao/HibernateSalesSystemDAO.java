package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.PurchaseItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {
    private final EntityManagerFactory emf;
    private final EntityManager em;
    long purchaseItemID = 0L;

    public HibernateSalesSystemDAO(){
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
    }

    void close(){
        em.close();
        emf.close();
    }

    @Override
    public List<StockItem> findStockItems() {
        List<StockItem> list;
        try {
            list =  em.createQuery("from StockItem", StockItem.class).getResultList();
        } catch (NoResultException e){
            list = new ArrayList<>();
        }
        return list;
    }

    @Override
    public StockItem findStockItem(long id) {
        StockItem item;
        try {
            item = em.createQuery("from StockItem where id='" + id + "'", StockItem.class).getSingleResult();
        } catch (NoResultException e){
            item = null;
        }
        return item;
    }

    @Override
    public StockItem findStockItem(String name) {
        StockItem item;
        try {
            item = em.createQuery("from StockItem where name='" + name + "'", StockItem.class).getSingleResult();
        } catch (NoResultException e){
            item = null;
        }
        return item;
    }

    @Override
    public List<SoldItem> findSoldItems() {
        try {
            return em.createQuery("from SoldItem", SoldItem.class).getResultList();
        } catch (NoResultException e){
            return null;
        }
    }

    @Override
    public SoldItem findSoldItem(long id) {
        try {
            return em.createQuery("from SoldItem where id='" + id + "'", SoldItem.class).getSingleResult();
        } catch (NoResultException e){
            return null;
        }
    }

    @Override
    public List<PurchaseItem> findPurchaseItems() {
        try {
            return em.createQuery("from PurchaseItem order by date desc, time desc", PurchaseItem.class).getResultList();
        } catch (NoResultException e){
            return null;
        }
    }

    @Override
    public PurchaseItem findPurchaseItem(long id) {
        try {
            return em.createQuery("from PurchaseItem where id='" + id + "'", PurchaseItem.class).getSingleResult();
        } catch (NoResultException e){
            return null;
        }
    }

    @Override
    public List<PurchaseItem> findLast10Purchases (){
        try {
            return em.createQuery("from PurchaseItem order by date desc, time desc", PurchaseItem.class).setMaxResults(10).getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List<PurchaseItem> findPurchasesBetweenDates(LocalDate from, LocalDate to) {
        try {
            return em.createQuery("from PurchaseItem where date>='"+from.toString()+"' and date<='"+to.toString()+"' order by date desc, time desc", PurchaseItem.class).setMaxResults(10).getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public void addStockItem(StockItem stockItem) {
        em.persist(stockItem);
    }

    @Override
    public void removeStockItem(Long id) {
        em.remove(findStockItem(id));
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        em.persist(item);
    }

    @Override
    public void savePurchaseItem(List<SoldItem> items) {
        purchaseItemID = getPurchaseItemId();
        PurchaseItem item = new PurchaseItem(purchaseItemID++,items);
        em.persist(item);
    }

    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }
    @Override
    public Long getPurchaseItemId(){
        while (findPurchaseItem(purchaseItemID)!=null)
            purchaseItemID++;
        return purchaseItemID;
    }
}
