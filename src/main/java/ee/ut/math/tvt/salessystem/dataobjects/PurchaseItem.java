package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;


/**
 * One purchase history entry
 */
@Entity
@Table(name="PURCHASEITEM")
public class PurchaseItem implements Comparable<PurchaseItem>{
    @Id
    private Long id;
    //@Transient
    @OneToMany
    @JoinTable(name="EMP_SOLDITEM",
            joinColumns=@JoinColumn(name="EMP_ID"),
            inverseJoinColumns=@JoinColumn(name="SOLDITEM_ID"))
    private List<SoldItem> soldItems;
    @Column(name = "date")
    private LocalDate date;
    @Column(name = "time")
    private LocalTime time;
    @Column(name = "total")
    private double total;

    public PurchaseItem() {
    }

    public PurchaseItem(Long id,List<SoldItem> soldItems) {
        this.id=id;
        this.soldItems=soldItems;
        this.date=java.time.LocalDate.now();
        this.time=java.time.LocalTime.now();
        double s=0;
        for (SoldItem si :soldItems){
            s+=si.getSum();
        }
        this.total=s;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<SoldItem> getSoldItems() {
        return soldItems;
    }

    public void setSoldItems(List<SoldItem> soldItems) {
        this.soldItems = soldItems;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time.truncatedTo(ChronoUnit.SECONDS);
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public double getTotal(){return total;}

    @Override
    public String toString() {
        return "PurchaseItem{" +
                "id=" + id +
                ", soldItems=" + soldItems.toString() +
                ", date=" + date +
                ", time=" + time +
                '}';
    }
    @Override
    public int compareTo( PurchaseItem b){
        String[] acomp=this.getDate().toString().split("-");
        String[] bcomp=b.getDate().toString().split("-");
        if(Integer.parseInt(acomp[0])-Integer.parseInt(bcomp[0])!=0){
            return Integer.parseInt(acomp[0])-Integer.parseInt(bcomp[0]);
        }
        if(Integer.parseInt(acomp[1])-Integer.parseInt(bcomp[1])!=0){
            return Integer.parseInt(acomp[1])-Integer.parseInt(bcomp[1]);
        }
        if(Integer.parseInt(acomp[2])-Integer.parseInt(bcomp[2])!=0){
            return Integer.parseInt(acomp[2])-Integer.parseInt(bcomp[2]);
        }
        return 0;
    }
    public int compareTo( LocalDate b){
        String[] acomp=this.getDate().toString().split("-");
        String[] bcomp=b.toString().split("-");
        if(Integer.parseInt(acomp[0])-Integer.parseInt(bcomp[0])!=0){
            return Integer.parseInt(acomp[0])-Integer.parseInt(bcomp[0]);
        }
        if(Integer.parseInt(acomp[1])-Integer.parseInt(bcomp[1])!=0){
            return Integer.parseInt(acomp[1])-Integer.parseInt(bcomp[1]);
        }
        if(Integer.parseInt(acomp[2])-Integer.parseInt(bcomp[2])!=0){
            return Integer.parseInt(acomp[2])-Integer.parseInt(bcomp[2]);
        }
        return 0;
    }
}
