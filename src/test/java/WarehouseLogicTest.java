import static org.junit.Assert.assertEquals;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.PurchaseItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.CartLogic;
import ee.ut.math.tvt.salessystem.logic.HistoryLogic;
import ee.ut.math.tvt.salessystem.logic.WarehouseLogic;
import org.junit.Test;

import java.util.Collections;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class WarehouseLogicTest {
    private final InMemorySalesSystemDAO dao;
    private final CartLogic shoppingLogic;
    private final WarehouseLogic warehouseLogic;
    private final HistoryLogic historyLogic;

    public WarehouseLogicTest() {
        dao = new InMemorySalesSystemDAO();
        warehouseLogic = new WarehouseLogic(dao);
        shoppingLogic = new CartLogic(dao, warehouseLogic);
        historyLogic = new HistoryLogic(dao);
    }

    @Test
    public void testAddingItemBeginsAndCommitsTransaction() throws SalesSystemException {
        StockItem item = new StockItem(12L, "New Item", "", 123.01, 321);
        warehouseLogic.addProduct(item);

        assert dao.testActionsMade.contains(4);
        assert dao.testActionsMade.contains(6);
        assert dao.testActionsMade.indexOf(4) < dao.testActionsMade.indexOf(6);
    }

    @Test
    public void testAddingNewItem() throws SalesSystemException {
        StockItem item = new StockItem(12L, "New Item", "", 123.01, 321);
        warehouseLogic.addProduct(item);
        assert dao.findStockItem(12L) != null;
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingNewItemWithNegativeQuantity() throws SalesSystemException {
        StockItem item = new StockItem(12L, "New Negative Item", "", 123.01, -321);
        warehouseLogic.addProduct(item);
        assert dao.findStockItem(12L) != null;
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingNewItemWithNoName() throws SalesSystemException {
        StockItem item = new StockItem(12L, "", "", 123.01, 321);
        warehouseLogic.addProduct(item);
        assert dao.findStockItem(12L) != null;
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingNewItemWithNegativeId() throws SalesSystemException {
        StockItem item = new StockItem(-12L, "A", "", 123.01, 321);
        warehouseLogic.addProduct(item);
        assert dao.findStockItem(12L) != null;
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingNewItemWithNegativePrice() throws SalesSystemException {
        StockItem item = new StockItem(12L, "A", "", -1.31, 321);
        warehouseLogic.addProduct(item);
        assert dao.findStockItem(12L) != null;
    }

    @Test
    public void testAddingExistingItem() throws SalesSystemException {
        StockItem item = new StockItem(dao.findStockItem(1L));
        int num = item.getQuantity();
        item.setQuantity(num+1);
        warehouseLogic.addProduct(item);
        assertEquals(2*num+1, dao.findStockItem(1L).getQuantity());

        assert Collections.frequency(dao.testActionsMade, 8) ==
                Collections.frequency(dao.testActionsMade, 10);
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingExistingItemWithWrongId() throws SalesSystemException {
        StockItem item = new StockItem(dao.findStockItem(1L));
        item.setQuantity(3);
        item.setId(item.getId() + 1000);
        warehouseLogic.addProduct(item);
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingExistingItemWithWrongName() throws SalesSystemException {
        StockItem item = new StockItem(dao.findStockItem(1L));
        item.setQuantity(3);
        item.setName(item.getName() + "NONONO");
        warehouseLogic.addProduct(item);
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingExistingItemWithCollidingId() throws SalesSystemException {
        StockItem item = new StockItem(1L,
                dao.findStockItem(1L).getName() + "A", "",
                dao.findStockItem(1L).getPrice() + 1.1, 1);
        warehouseLogic.addProduct(item);
        assert dao.findStockItem(12L) != null;
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingExistingItemWithCollidingName() throws SalesSystemException {
        StockItem item = new StockItem(1313L,
                dao.findStockItem(1L).getName(), "",
                dao.findStockItem(1L).getPrice() + 1.1, 1);
        warehouseLogic.addProduct(item);
    }

    @Test
    public void testAddingExistingItemWithNegativeQuantityOk() throws SalesSystemException {
        StockItem item = new StockItem(dao.findStockItem(1L));
        item.setQuantity( -item.getQuantity());
        warehouseLogic.addProduct(item);
        assert dao.findStockItem(1L) == null;
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingExistingItemWithNegativeQuantityFail() throws SalesSystemException {
        StockItem item = new StockItem(dao.findStockItem(1L));
        item.setQuantity( -item.getQuantity() - 1);
        warehouseLogic.addProduct(item);
    }
}
