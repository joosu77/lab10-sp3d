import static org.junit.Assert.assertEquals;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.PurchaseItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.CartLogic;
import ee.ut.math.tvt.salessystem.logic.HistoryLogic;
import ee.ut.math.tvt.salessystem.logic.WarehouseLogic;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;

public class CartLogicTest {
    private final InMemorySalesSystemDAO dao;
    private final CartLogic shoppingLogic;
    private final WarehouseLogic warehouseLogic;
    private final HistoryLogic historyLogic;

    public CartLogicTest() {
        dao = new InMemorySalesSystemDAO();
        warehouseLogic = new WarehouseLogic(dao);
        shoppingLogic = new CartLogic(dao, warehouseLogic);
        historyLogic = new HistoryLogic(dao);
    }

    @Test
    public void testAddingNewItem() {
        StockItem one = dao.findStockItem(1);
        shoppingLogic.addItem(one, 2);

        assert shoppingLogic.getAll().get(0).getQuantity() == 2;
        assert shoppingLogic.getAll().size() == 1;

        StockItem two = dao.findStockItem(2);
        shoppingLogic.addItem(two, 3);

        assert shoppingLogic.getAll().size() == 2;
        assert shoppingLogic.getAll().stream().filter(e -> e.getId() == 1).findAny().get().getQuantity() == 2;
        assert shoppingLogic.getAll().stream().filter(e -> e.getId() == 2).findAny().get().getQuantity() == 3;
    }

    @Test
    public void testAddingExistingItem() {
        StockItem one = dao.findStockItem(1);
        shoppingLogic.addItem(one, 1);
        StockItem two = dao.findStockItem(1); // Making sure there are no shenanigans whatsoever
        shoppingLogic.addItem(two, 3);

        assert shoppingLogic.getAll().get(0).getQuantity() == 4;
    }

    @Test
    public void testAddingItemWithNegativeQuantityToPositive(){
        StockItem one = dao.findStockItem(1);
        shoppingLogic.addItem(one, 3);
        StockItem two = dao.findStockItem(1);
        shoppingLogic.addItem(two, -2);

        assert shoppingLogic.getAll().size() == 1;
        assert shoppingLogic.getAll().get(0).getQuantity() == 1;
    }

    @Test
    public void testAddingItemWithNegativeQuantityToZero(){
        StockItem one = dao.findStockItem(1);
        shoppingLogic.addItem(one, 3);
        StockItem two = dao.findStockItem(1);
        shoppingLogic.addItem(two, -3);

        assert shoppingLogic.getAll().size() == 0;
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithNegativeQuantityToNegative() throws SalesSystemException {
        StockItem one = dao.findStockItem(1);
        shoppingLogic.addItem(one, 1);
        StockItem two = dao.findStockItem(1);
        shoppingLogic.addItem(two, -2);
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithQuantityTooLargeNew(){
        StockItem one = dao.findStockItem(1);
        shoppingLogic.addItem(one, 999);
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithQuantityTooLargeExisting(){
        StockItem one = dao.findStockItem(1);
        shoppingLogic.addItem(one, 1);
        StockItem two = dao.findStockItem(1);
        shoppingLogic.addItem(two, 999);
    }

    @Test
    public void testRemovingItem() {
        StockItem two = dao.findStockItem(2);
        shoppingLogic.addItem(two, 3);

        StockItem one = dao.findStockItem(1);
        shoppingLogic.addItem(one, 4);

        shoppingLogic.removeItem(1L);

        shoppingLogic.submitCurrentPurchase();

        PurchaseItem created = historyLogic.getAll().stream().findAny().get();
        assert created.getSoldItems().size() == 1;
        assert created.getSoldItems().stream().allMatch(e -> e.getQuantity() == e.getId() + 1);
    }

    @Test
    public void testRemovingItemDoNotAffectLaterAdding() {
        StockItem two = dao.findStockItem(2);
        shoppingLogic.addItem(two, 3);

        StockItem one = dao.findStockItem(1);
        shoppingLogic.addItem(one, 4);

        shoppingLogic.removeItem(1L);
        one = dao.findStockItem(1);
        shoppingLogic.addItem(one, 2);

        shoppingLogic.submitCurrentPurchase();

        PurchaseItem created = historyLogic.getAll().stream().findAny().get();
        assert created.getSoldItems().size() == 2;
        assert created.getSoldItems().stream().allMatch(e -> e.getQuantity() == e.getId() + 1);
    }

    @Test
    public void testSubmittingEmptyPurchase(){
        // This is a feature
        StockItem one = dao.findStockItem(1);
        shoppingLogic.addItem(one, 4);
        shoppingLogic.removeItem(1L);
        shoppingLogic.submitCurrentPurchase();
    }

    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity(){
        StockItem one = dao.findStockItem(1);
        shoppingLogic.addItem(one, 2);
        StockItem two = dao.findStockItem(2);
        shoppingLogic.addItem(two, 2);
        shoppingLogic.submitCurrentPurchase();

        assert warehouseLogic.findStockItem(1L).getQuantity() == 5 - 2;
        assert warehouseLogic.findStockItem(2L).getQuantity() == 8 - 2;
    }

    @Test
    public void testSubmittingCurrentPurchaseRemovesStockItem(){
        // This is a good test
        StockItem one = dao.findStockItem(1);
        shoppingLogic.addItem(one, 5);
        StockItem two = dao.findStockItem(2);
        shoppingLogic.addItem(two, 2);
        shoppingLogic.submitCurrentPurchase();

        PurchaseItem created = historyLogic.getAll().stream().findAny().get();
        assert warehouseLogic.findStockItem(1L) == null;
        assert warehouseLogic.findStockItem(2L).getQuantity() == 8 - 2;

        assert created.getSoldItems().stream().allMatch(e -> e.getId() != 1 || (
                e.getName().equals(one.getName()) && e.getPrice() == one.getPrice() &&
                        e.getQuantity() == 5 && e.getId() == 1

        ));
    }

    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction() {
        StockItem one = dao.findStockItem(1);
        shoppingLogic.addItem(one, 2);
        shoppingLogic.submitCurrentPurchase();

        assert dao.testActionsMade.get(0) == 4;
        assert dao.testActionsMade.get(dao.testActionsMade.size() - 1) == 6;
        assert Collections.frequency(dao.testActionsMade, 4) == 1;
        assert Collections.frequency(dao.testActionsMade, 6) == 1;
    }

    @Test
    public void testSubmittingCurrentOrderCreatesHistoryItem() {
        StockItem one = dao.findStockItem(1);
        shoppingLogic.addItem(one, 2);
        StockItem two = dao.findStockItem(2);
        shoppingLogic.addItem(two, 3);
        shoppingLogic.submitCurrentPurchase();

        PurchaseItem created = historyLogic.getAll().stream().findAny().get();
        assert created.getSoldItems().size() == 2;
        assert created.getSoldItems().stream().allMatch(e -> e.getQuantity() == e.getId() + 1 &&
                e.getName().equals(dao.findStockItem(e.getId()).getName()) &&
                e.getPrice() == dao.findStockItem(e.getId()).getPrice() &&
                e.getSum() == dao.findStockItem(e.getId()).getPrice() * e.getQuantity()
        );
        double total = 0.0;
        for (SoldItem a : created.getSoldItems()) total += a.getSum();
        assert created.getTotal() == total;
    }

    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime() {
        LocalDate startDate = LocalDate.now();
        LocalTime startTime = LocalTime.now().truncatedTo(ChronoUnit.SECONDS);;

        StockItem one = dao.findStockItem(1);
        shoppingLogic.addItem(one, 2);
        shoppingLogic.submitCurrentPurchase();

        LocalDate endDate = LocalDate.now();
        LocalTime endTime = LocalTime.now().truncatedTo(ChronoUnit.SECONDS);

        PurchaseItem created = historyLogic.getAll().stream().findAny().get();
        assert !created.getDate().isBefore(startDate);
        assert !created.getTime().isBefore(startTime);
        assert !created.getDate().isAfter(endDate);
        assert !created.getTime().isAfter(endTime);
    }

    @Test
    public void testCancellingOrder() {
        StockItem notone = dao.findStockItem(1);
        shoppingLogic.addItem(notone, 2);
        StockItem nottwo = dao.findStockItem(3);
        shoppingLogic.addItem(nottwo, 2);
        shoppingLogic.cancelCurrentPurchase();

        // Why not reuse?
        StockItem one = dao.findStockItem(1);
        shoppingLogic.addItem(one, 2);
        StockItem two = dao.findStockItem(2);
        shoppingLogic.addItem(two, 3);
        shoppingLogic.submitCurrentPurchase();

        PurchaseItem created = historyLogic.getAll().stream().findAny().get();
        assert created.getSoldItems().size() == 2;
        assert created.getSoldItems().stream().allMatch(e -> e.getQuantity() == e.getId() + 1);
        assert created.getSoldItems().stream().allMatch(e -> e.getName().equals(dao.findStockItem(e.getId()).getName()));
    }

    @Test
    public void testCancellingOrderQuanititesUnchanged() {
        int qone = warehouseLogic.findStockItem(1L).getQuantity();
        int qtwo = warehouseLogic.findStockItem(2L).getQuantity();

        StockItem notone = dao.findStockItem(1);
        shoppingLogic.addItem(notone, 2);
        StockItem nottwo = dao.findStockItem(2);
        shoppingLogic.addItem(nottwo, 2);
        shoppingLogic.cancelCurrentPurchase();

        assert qone == warehouseLogic.findStockItem(1L).getQuantity();
        assert qtwo == warehouseLogic.findStockItem(2L).getQuantity();
    }
}
