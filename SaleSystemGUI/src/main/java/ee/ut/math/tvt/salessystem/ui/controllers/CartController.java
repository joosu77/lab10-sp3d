package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.CartLogic;
import ee.ut.math.tvt.salessystem.ui.NewComboBox;
import ee.ut.math.tvt.salessystem.ui.UupsiWhoopsieLoudSpeaker;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "Point-of-sale" in the menu). Consists of the purchase menu,
 * current purchase dialog and shopping cart table.
 */
public class CartController implements Initializable {

    private static final Logger log = LogManager.getLogger(CartController.class);

    private final CartLogic logic;

    @FXML
    private Button newPurchase, submitPurchase, cancelPurchase;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private NewComboBox nameField;
    @FXML
    private TextField priceField;
    @FXML
    private Button addItemButton, removeItemButton;
    @FXML
    private TableView<SoldItem> purchaseTableView;

    public CartController(CartLogic logic) {
        this.logic = logic;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        purchaseTableView.setItems(FXCollections.observableList(logic.getAll()));
        disableProductField(true);

        ObservableList<String> items = FXCollections.observableArrayList(
                logic.findStockItems().stream().map(StockItem::getName).collect(Collectors.toList())
        );
        nameField.setAllItems(items);

        barCodeField.setOnKeyReleased(e -> barCodeChange());
        nameField.setOnAction(e -> comboChange()); // TODO fix weird caret movement

        removeItemButton.setStyle("-fx-font-size:10");
    }

    public void comboChange(){
        fillInputsBySelectedName();
    }

    public void barCodeChange(){
        fillInputsBySelectedStockItem();
    }

    /** Event handler for the <code>new purchase</code> event. */
    @FXML
    protected void newPurchaseButtonClicked() {
        log.info("New sale process started");
        try {
            enableInputs();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>cancel purchase</code> event.
     */
    @FXML
    protected void cancelPurchaseButtonClicked() {
        log.info("Sale cancelled");
        try {
            logic.cancelCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>submit purchase</code> event.
     */
    @FXML
    protected void submitPurchaseButtonClicked() {
        log.info("Sale complete");
        try {
            log.debug("Contents of the current basket:\n" + logic.getAll());
            logic.submitCurrentPurchase();
            disableInputs();
            purchaseTableView.refresh();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    // switch UI to the state that allows to proceed with the purchase
    private void enableInputs() {
        resetProductField();
        disableProductField(false);
        cancelPurchase.setDisable(false);
        submitPurchase.setDisable(false);
        newPurchase.setDisable(true);
        priceField.setEditable(false);
    }

    // switch UI to the state that allows to initiate new purchase
    private void disableInputs() {
        resetProductField();
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        newPurchase.setDisable(false);
        disableProductField(true);
    }

    private void fillInputsBySelectedStockItem() {
        StockItem stockItem = getStockItemByBarcode();
        if (stockItem != null) {
            nameField.setValue(stockItem.getName());
            priceField.setText(String.valueOf(stockItem.getPrice()));
        } else {
            //resetProductField();
        }
    }

    private void fillInputsBySelectedName(){
        StockItem stockItem = getStockItemByName();
        if (stockItem != null) {
            priceField.setText(String.valueOf(stockItem.getPrice()));
            barCodeField.setText(String.valueOf(stockItem.getId()));
        } else {
            //resetProductField();
        }
    }

    // Search the warehouse for a StockItem with the bar code entered
    // to the barCode textfield.
    private StockItem getStockItemByBarcode() {
        try {
            long code = Long.parseLong(barCodeField.getText());
            return logic.findStockItem(code);
        } catch (NumberFormatException e) {
            return null;
        }
    }
    private StockItem getStockItemByName() {
        try {
            String name = nameField.getValue();
            //String name = ComboBoxAutoComplete.getComboBoxValue(nameField);
            return logic.findStockItem(name);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    /**
     * Add new item to the cart.
     */
    @FXML
    public void addItemEventHandler() {
        try {
            int quantity;
            if (quantityField.getText().equals("")) quantity = 1;
            else quantity = Integer.parseInt(quantityField.getText());
            // if (quantity == 0) new UupsiWhoopsieLoudSpeaker().setE(
            //         new Exception("Should I throw an error because of the quantity? Nah, not this time!")).naita();

            StockItem stockItem = getStockItemByBarcode();
            StockItem stockItem2 = getStockItemByName();

            if (stockItem != stockItem2) throw new SalesSystemException("Name and barcode do not match");
            if (stockItem == null) throw new SalesSystemException("Error when parsing barcode");

            logic.addItem(stockItem, quantity);
        } catch (Exception e) {
            new UupsiWhoopsieLoudSpeaker().setE(e).naita();
        }
        purchaseTableView.refresh();
    }

    /**
     * Remove an item from the cart.
     */
    @FXML
    public void removeItemEventHandler() {
        try {
            StockItem stockItem = getStockItemByBarcode();
            StockItem stockItem2 = getStockItemByName();

            if (stockItem != stockItem2) throw new SalesSystemException("Name and barcode do not match");
            if (stockItem == null) throw new SalesSystemException("Error when parsing barcode");

            logic.removeItem(stockItem.getId());
        } catch (Exception e) {
            new UupsiWhoopsieLoudSpeaker().setE(e).naita();
        }
        purchaseTableView.refresh();
    }

    /**
     * Sets whether or not the product component is enabled.
     */
    private void disableProductField(boolean disable) {
        this.addItemButton.setDisable(disable);
        this.barCodeField.setDisable(disable);
        this.quantityField.setDisable(disable);
        this.nameField.setDisable(disable);
        this.priceField.setDisable(disable);
    }

    /**
     * Reset dialog fields.
     */
    private void resetProductField() {
        barCodeField.setText("");
        quantityField.setText("1");
        nameField.setValue("");
        priceField.setText("");
    }

    public void onSelected() {
        ObservableList<String> items = FXCollections.observableArrayList(
                logic.findStockItems().stream().map(StockItem::getName).collect(Collectors.toList())
        );
        nameField.setAllItems(items);
        nameField.refresh();
    }
}
