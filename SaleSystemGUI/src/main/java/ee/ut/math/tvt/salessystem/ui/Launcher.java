package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;

public class Launcher {
    public static void main(String[] args) {
        new SalesSystemUI().main(args);
    }
}
