package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.WarehouseLogic;
import ee.ut.math.tvt.salessystem.ui.UupsiWhoopsieLoudSpeaker;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class WarehouseController implements Initializable {

    private final WarehouseLogic logic;
    private static final Logger log = LogManager.getLogger(WarehouseController.class);

    @FXML
    private TextField barcode, amount, name, price;
    @FXML
    private Button addItem, removeItem;
    @FXML
    private Button refresh;
    @FXML
    private TableView<StockItem> warehouseTableView;

    public WarehouseController(WarehouseLogic logic) {
        this.logic = logic;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshStockItems();

        addItem.setOnMouseClicked(e -> addItemButtonClicked());
        removeItem.setOnMouseClicked(e -> removeItemButtonClicked());
        refresh.setOnMouseClicked(e -> refreshStockItems());
        barcode.setOnKeyReleased(e -> barCodeChange());
        name.setOnKeyReleased(e -> nameChange());
        // TODO refresh view after adding new items

        // https://bugs.openjdk.java.net/browse/JDK-8089280
        // Won't fix

    }

    public void barCodeChange(){
        try {
            StockItem stockItem = logic.findStockItem(Long.parseLong(barcode.getText()));
            if (stockItem != null) {
                name.setText(stockItem.getName());
                price.setText(String.valueOf(stockItem.getPrice()));
            }
        } catch (SalesSystemException ignored) {}
    }

    public void nameChange(){
        try {
            StockItem stockItem = logic.findStockItem(name.getText());
            if (stockItem != null) {
                barcode.setText(String.valueOf(stockItem.getId()));
                price.setText(String.valueOf(stockItem.getPrice()));
            }
        } catch (SalesSystemException ignored) {}
    }

    @FXML
    public void addItemButtonClicked() {
        try {
            StockItem item = new StockItem(
                    Long.parseLong(this.barcode.getText()),
                    this.name.getText(),
                    "",  // TODO
                    Math.round(Double.parseDouble(this.price.getText()) * 100) / 100.0,
                    Integer.parseInt(this.amount.getText()));

            logic.addProduct(item);
        } catch (Exception e) {
            new UupsiWhoopsieLoudSpeaker().setE(e).naita();
        }

        refreshStockItems();
    }

    public void refreshStockItems() {
        warehouseTableView.setItems(FXCollections.observableList(logic.findStockItems()));
        log.info("Loaded "+logic.findStockItems().size()+" items into the warehouse.");

        warehouseTableView.refresh();
    }

    private void removeItemButtonClicked() {
        try {
            logic.removeStockItem(Long.parseLong(barcode.getText()));
        } catch (SalesSystemException e){
            new UupsiWhoopsieLoudSpeaker().setE(e).naita();
        }
        refreshStockItems();
    }
}
