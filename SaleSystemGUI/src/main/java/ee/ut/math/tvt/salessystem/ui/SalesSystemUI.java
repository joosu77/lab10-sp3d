package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.logic.HistoryLogic;
import ee.ut.math.tvt.salessystem.logic.WarehouseLogic;
import ee.ut.math.tvt.salessystem.ui.controllers.HistoryController;
import ee.ut.math.tvt.salessystem.ui.controllers.CartController;
import ee.ut.math.tvt.salessystem.ui.controllers.WarehouseController;
import ee.ut.math.tvt.salessystem.logic.CartLogic;
import ee.ut.math.tvt.salessystem.ui.controllers.DevelopersController;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;

/**
 * Graphical user interface of the sales system.
 */
public class SalesSystemUI extends Application {

    private static final Logger log = LogManager.getLogger(SalesSystemUI.class);

    private final SalesSystemDAO dao;
    private final CartLogic shoppingLogic;
    private final WarehouseLogic warehouseLogic;
    private final HistoryLogic historyLogic;

    public SalesSystemUI() {
        dao = new InMemorySalesSystemDAO();
        //dao = new HibernateSalesSystemDAO();
        warehouseLogic = new WarehouseLogic(dao);
        shoppingLogic = new CartLogic(dao, warehouseLogic);
        historyLogic = new HistoryLogic(dao);
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        log.info("javafx version: " + System.getProperty("javafx.runtime.version"));

        Tab cartTab = new Tab();
        cartTab.setText("Point-of-sale");
        cartTab.setClosable(false);
        CartController purchaseController = new CartController(shoppingLogic);
        cartTab.setContent(loadControls("CartTab.fxml", purchaseController));

        Tab warehouseTab = new Tab();
        warehouseTab.setText("Warehouse");
        warehouseTab.setClosable(false);
        WarehouseController warehouseController = new WarehouseController(warehouseLogic);
        warehouseTab.setContent(loadControls("WarehouseTab.fxml", warehouseController));

        Tab historyTab = new Tab();
        historyTab.setText("History");
        historyTab.setClosable(false);
        historyTab.setContent(loadControls("HistoryTab.fxml", new HistoryController(historyLogic))); // TODO

        Tab teamTab = new Tab();
        teamTab.setText("Developers");
        teamTab.setClosable(false);
        teamTab.setContent(loadControls("DevelopersTab.fxml", new DevelopersController())); // TODO

        Group root = new Group();
        Scene scene = new Scene(root, 600, 500, Color.WHITE);
        scene.getStylesheets().add(getClass().getResource("DefaultTheme.css").toExternalForm());

        BorderPane borderPane = new BorderPane();
        borderPane.prefHeightProperty().bind(scene.heightProperty());
        borderPane.prefWidthProperty().bind(scene.widthProperty());

        TabPane tabPane = new TabPane(cartTab, warehouseTab, historyTab, teamTab);
        tabPane.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (newValue == cartTab) purchaseController.onSelected();
                    if (newValue == warehouseTab) warehouseController.refreshStockItems();
                }
        );
        borderPane.setCenter(tabPane);
        root.getChildren().add(borderPane);

        primaryStage.setTitle("Sales system");
        primaryStage.setScene(scene);
        primaryStage.show();

        log.info("Salesystem GUI started");
    }

    private Node loadControls(String fxml, Initializable controller) throws IOException {
        URL resource = getClass().getResource(fxml);
        if (resource == null)
            throw new IllegalArgumentException(fxml + " not found");

        FXMLLoader fxmlLoader = new FXMLLoader(resource);
        fxmlLoader.setController(controller);
        return fxmlLoader.load();
    }
}


