package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dataobjects.PurchaseItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.logic.HistoryLogic;
import ee.ut.math.tvt.salessystem.ui.UupsiWhoopsieLoudSpeaker;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryController implements Initializable {
    private static final Logger log = LogManager.getLogger(HistoryController.class);

    private final HistoryLogic logic;

    public HistoryController(HistoryLogic logic) {
        this.logic = logic;
    }

    @FXML
    private Button showBetweenDates;
    @FXML
    private Button showLastTen;
    @FXML
    private Button showAll;
    @FXML
    private TableView<PurchaseItem> historyTableView;
    @FXML
    private TableView<SoldItem> dealTableView;
    @FXML
    private DatePicker startDate;
    @FXML
    private DatePicker endDate;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        showBetweenDates.setOnMouseClicked(e -> showBetweenDatesButtonClicked());
        showLastTen.setOnMouseClicked(e -> ShowLastTenButtonClicked());
        showAll.setOnMouseClicked(e -> showAllButtonClicked());
        historyTableView.setOnMouseClicked(e -> fillSelectedTable(historyTableView.getSelectionModel().getSelectedItem()));
        historyTableView.refresh();
        dealTableView.refresh();
        log.info("Loaded history tab");
    }

    @FXML
    protected void showBetweenDatesButtonClicked() {
        try {
            if (startDate.getValue() == null || endDate.getValue() == null)
                throw new SalesSystemException("First pick the dates");

            List<PurchaseItem> items = logic.getBetweenDates(startDate.getValue(), endDate.getValue());
            fillPurchaseTable(items);
        } catch (Exception e) {
            new UupsiWhoopsieLoudSpeaker().setE(e).naita();
        }
    }

    @FXML
    protected void ShowLastTenButtonClicked() {
        try {
            List<PurchaseItem> items = logic.getLastTen();
            fillPurchaseTable(items);
        } catch (Exception e) {
            new UupsiWhoopsieLoudSpeaker().setE(e).naita();
        }
    }

    @FXML
    protected void showAllButtonClicked() {
        try {
            List<PurchaseItem> items = logic.getAll();
            fillPurchaseTable(items);
        } catch (Exception e) {
            new UupsiWhoopsieLoudSpeaker().setE(e).naita();
        }
    }

    protected void fillPurchaseTable(List<PurchaseItem> items) {
        historyTableView.setItems(FXCollections.observableList(items));
        dealTableView.setItems(FXCollections.observableList(new ArrayList<SoldItem>()));
        historyTableView.refresh();
        dealTableView.refresh();
    }

    @FXML
    protected void fillSelectedTable(PurchaseItem purchase){
        if (purchase!=null) { // if purchase is null then the click was not on any row of the table and should be ignored
            dealTableView.setItems(FXCollections.observableList(purchase.getSoldItems()));
            dealTableView.refresh();
        }
    }
}
