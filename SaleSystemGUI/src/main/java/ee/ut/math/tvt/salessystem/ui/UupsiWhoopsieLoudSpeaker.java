package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.ui.controllers.DevelopersController;
import javafx.scene.control.Alert;
import javafx.scene.layout.Region;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UupsiWhoopsieLoudSpeaker {
    Exception e = null;
    private static final Logger log = LogManager.getLogger(UupsiWhoopsieLoudSpeaker.class);

    public Exception getE() {
        return e;
    }

    public UupsiWhoopsieLoudSpeaker setE(Exception e) {
        this.e = e;
        return this;
    }

    public void naita() {
        if (e == null) {
            e = new Exception("Exception: no exceptions");
        }

        log.error(e.toString());

        Alert errorAlert = new Alert(Alert.AlertType.ERROR);
        errorAlert.setHeaderText("Whoopsie! You messed up!");
        String text = "";
        switch (e.getClass().getCanonicalName()) {
            case "java.lang.NumberFormatException": text = "Unable to convert to number"; break;
        }
        text += "\n" + e.getMessage();
        errorAlert.setContentText(text.length() <= 4 ?
                "An obvious mistake many of us had made! Do not worry and try just one more time" : text);

        errorAlert.setResizable(true);  // Fixes 1-pixel width popups in KDE
        errorAlert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        errorAlert.showAndWait();

        LogManager.getLogger(UupsiWhoopsieLoudSpeaker.class).error(text);
    }
}
