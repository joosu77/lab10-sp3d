package ee.ut.math.tvt.salessystem.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import javax.enterprise.inject.New;
import java.util.List;

public class NewComboBox extends ComboBox<String>{
    List<String> AllItems;

    public NewComboBox (){
        this(FXCollections.observableArrayList());
    }

    public NewComboBox (ObservableList<String> items){
        super(items);
        super.setEditable(true);
        AllItems = items;
        super.getEditor().focusedProperty().addListener(observable -> {
            if (super.getSelectionModel().getSelectedIndex() < 0) {
                super.getEditor().setText(null);
            }
        });
        super.getEditor().addEventHandler(KeyEvent.KEY_PRESSED, t-> super.hide());
        super.getEditor().addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<>(){

            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.ENTER){
                    NewComboBox.super.hide();
                } else if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT){
                    String written = NewComboBox.super.getEditor().getText();
                    int carpos = NewComboBox.super.getEditor().getCaretPosition();
                    NewComboBox.super.getSelectionModel().clearSelection();
                    NewComboBox.super.getEditor().setText(written);
                    if (event.getCode() == KeyCode.LEFT){
                        carpos = carpos==0?carpos:carpos-1;
                    } else {
                        carpos = carpos==written.length()?carpos:carpos+1;
                    }
                    NewComboBox.super.getEditor().positionCaret(carpos);
                } else if (event.getCode() == KeyCode.UP || event.getCode() == KeyCode.DOWN){
                    NewComboBox.super.getEditor().positionCaret(NewComboBox.super.getEditor().getText().length());
                } else {
                    refresh();
                    String written = NewComboBox.super.getEditor().getText();
                    int carpos = NewComboBox.super.getEditor().getCaretPosition();
                    NewComboBox.super.getSelectionModel().clearSelection();
                    NewComboBox.super.getEditor().setText(written);
                    NewComboBox.super.getEditor().positionCaret(carpos);
                }
            }
        });
    }

    public void refresh(){
        ObservableList<String> out = FXCollections.observableArrayList();
        for (String s: AllItems){
            if (super.getEditor().getText() != null && s.toLowerCase().contains(super.getEditor().getText().toLowerCase())){
                out.add(s);
            }
        }
        if (out.size()==0){
            super.hide();
        } else {
            super.setItems(out);
            super.show();
        }
    }

    public void setAllItems(List<String> items){
        AllItems = items;
    }

}
