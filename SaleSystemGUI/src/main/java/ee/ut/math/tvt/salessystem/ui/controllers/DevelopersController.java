package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.logic.PropertyValues;
import ee.ut.math.tvt.salessystem.ui.UupsiWhoopsieLoudSpeaker;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ResourceBundle;

public class DevelopersController implements Initializable {
    private static final Logger log = LogManager.getLogger(DevelopersController.class);

    @FXML
    private Text teamName;
    @FXML
    private Text email;
    @FXML
    private Text leader;
    @FXML
    private Text member1;
    @FXML
    private Text member2;
    @FXML
    private Text member3;
    @FXML
    private ImageView logo;

    int rahu = 5;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        PropertyValues prop = new PropertyValues();
        teamName.setText(prop.teamName);
        email.setText(prop.email);
        leader.setText(prop.leader);
        Text t[] = {member1,member2,member3};
        for (int i=0; i<prop.getMemnum();i++) {
            t[i].setText(prop.members.get(i));
        }

        updatelogo(false);
        logo.setX(200);
        logo.setY(200);
        logo.setOnMouseEntered(e -> {updatelogo(true);});
        logo.setOnMouseExited(e -> {updatelogo(false);});
        logo.setOnMouseClicked(e -> {
            rahu--;
            if (rahu < 0 && rahu > -5)
                new UupsiWhoopsieLoudSpeaker().naita();
            if (rahu < -15)
                new UupsiWhoopsieLoudSpeaker().setE(new Exception("How Did We Get Here?")).naita();
        });
        log.info("Loaded team tab");

    }

    private void updatelogo(Boolean isFull) {
        Image logoImage = null;
        try {
            logoImage = new Image(new FileInputStream(isFull ? "./res/logo.png" : "./res/logo-minimal.png"));
        } catch (FileNotFoundException e) {
            try {
                logoImage = new Image(new FileInputStream(isFull ? "../res/logo.png" : "../res/logo-minimal.png"));
            } catch (FileNotFoundException fileNotFoundException) {
                e.printStackTrace();
            }
        }
        logo.setImage(logoImage);
    }
}
