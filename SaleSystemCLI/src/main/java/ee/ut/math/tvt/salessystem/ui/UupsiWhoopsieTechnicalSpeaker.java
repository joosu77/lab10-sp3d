package ee.ut.math.tvt.salessystem.ui;

import org.apache.logging.log4j.LogManager;

public class UupsiWhoopsieTechnicalSpeaker {
    Exception e = null;

    public Exception getE() {
        return e;
    }

    public UupsiWhoopsieTechnicalSpeaker setE(Exception e) {
        this.e = e;
        return this;
    }
    public void naita(){
        if (e == null) {
            e = new Exception("Exception: no exceptions");
        }
        String text = "";
        switch (e.getClass().getCanonicalName()) {
            case "java.lang.NumberFormatException": text = "Unable to convert to number\n"; break;
        }
        text += e.getMessage();
        text = (text.length() <= 4 ? "An obvious mistake many of us had made! Do not worry and try just one more time" : text);

        System.out.println("<>E><R<><R<><><>O><><><><><>R><>");
        System.out.println("Whoopsie! You messed up!");
        System.out.println(text);
        System.out.println("<><>E><><><><><>R><><><R<><O<>R>");

        LogManager.getLogger(UupsiWhoopsieTechnicalSpeaker.class).error(text);
    }
}
