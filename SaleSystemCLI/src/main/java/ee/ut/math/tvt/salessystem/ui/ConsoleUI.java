package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.PurchaseItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.HistoryLogic;
import ee.ut.math.tvt.salessystem.logic.PropertyValues;
import ee.ut.math.tvt.salessystem.logic.CartLogic;
import ee.ut.math.tvt.salessystem.logic.WarehouseLogic;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);

    private final SalesSystemDAO dao;
    private final CartLogic cartLogic;
    private final WarehouseLogic warehouseLogic;
    private final HistoryLogic historyLogic;


    public ConsoleUI(SalesSystemDAO dao) {
        this.dao = dao;
        warehouseLogic = new WarehouseLogic(dao);
        cartLogic = new CartLogic(dao,warehouseLogic);
        historyLogic=new HistoryLogic(dao);
    }

    public static void main(String[] args) throws Exception {
        //SalesSystemDAO dao = new HibernateSalesSystemDAO();
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();
    }

    /**
     * Run the sales system CLI.
     */
    public void run() throws IOException {
        log.info("Loaded CLI application");
        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");
        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase());
        }
    }

    private void showStock() {
        List<StockItem> stockItems = warehouseLogic.findStockItems();
        for (StockItem si : stockItems) {
            System.out.println(si.getId() + " " + si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (stockItems.size() == 0) System.out.println("\tNothing");
    }

    private void showCart() {
        List<SoldItem> elems = cartLogic.getAll();
        for (SoldItem si : elems) {
            System.out.println(si.getName() + " " + si.getPrice() + "€ (" + si.getQuantity() + " remaining)");
        }
        if (elems.size() == 0) System.out.println("\tNothing");
    }

    private void showTeam() {
        PropertyValues prop = new PropertyValues();
        for (String s : prop.logoascii)
            System.out.println("    "+s);
        System.out.println("\nTeam name: "+prop.teamName);
        System.out.println("Team leader: "+prop.leader+" "+prop.email);
        System.out.println("Team members: "+"");
        for (int i=0;i<prop.getMemnum();i++){
            System.out.println("    "+prop.members.get(i));
        }
    }

    private void showPurchase(String id) {
        List<SoldItem> elems = historyLogic.findPurchaseItem(Long.parseLong(id)).getSoldItems();
        for(SoldItem elem: elems){
            System.out.println("Id:" + elem.getId() + " Name:\"" + elem.getName() + "\" Price:" + elem.getPrice() +
                    "€ Quantity:" + elem.getQuantity() + " Sum:" + elem.getSum() + "€");
        }
        if (elems.size() == 0) System.out.println("\tNothing");
    }

    private String purchaseItemRepresentation(PurchaseItem p) {
        return "Id:" + p.getId() + " Date:" + p.getDate() +
                " Time:" + p.getTime() + " Total:" + p.getTotal() + "€";
    }

    private void showAllHistory() {
        for(PurchaseItem elem:historyLogic.getAll()){
            System.out.println(purchaseItemRepresentation(elem));
        }
        if (historyLogic.getAll().size() == 0) System.out.println("\tNothing");
    }

    private void show10History() {
        List<PurchaseItem> elems = historyLogic.getLastTen();
        for(PurchaseItem elem: elems){
            System.out.println(purchaseItemRepresentation(elem));
        }
        if (elems.size() == 0) System.out.println("\tNothing");
    }

    private void showBetweenHistory(String from, String to) {
        List<PurchaseItem> elems = historyLogic.getBetweenDates(java.time.LocalDate.parse(from),java.time.LocalDate.parse(to));
        for(PurchaseItem elem: elems){
            System.out.println(purchaseItemRepresentation(elem));
        }
        if (elems.size() == 0) System.out.println("\tNothing");
    }

    private void printUsage() {
        System.out.println("Usage:");
        System.out.println("h\t\tShow this help");
        System.out.println("q\t\tExit the program");

        System.out.println("c\t\tShow cart contents");
        System.out.println("ac IDX NR \tAdd NR of stock item with index IDX to the cart");
        System.out.println("rc IDX \tRemove all stock items with index IDX from the cart");
        System.out.println("p\t\tPurchase the shopping cart");
        System.out.println("r\t\tReset the shopping cart");

        System.out.println("w\t\tShow warehouse contents");
        System.out.println("ap IDX NR NAME PRC \tAdd NR of item with index IDX, name NAME and price PRC to warehouse");
        System.out.println("rp IDX NR \tRemove all items with index IDX from warehouse");

        System.out.println("sa\t\tShow all purchase history.");
        System.out.println("s10\t\tShow 10 most recent purchases.");
        System.out.println("s FROM TO\t\tShow purchase history between the dates FROM and TO, where From and TO use format yyyy-MM-dd.");
        System.out.println("sp ID\t\tShow purchase with id ID");

        System.out.println("t\t\tShow team tab contents");
    }

    private void processCommand(String command) {
        String[] c = command.split(" ");

        List<String> decorateCommands = List.of("h", "help", "?", "c", "w", "sa", "s10", "s", "sp", "t");
        String decorateString = "-------------------------";

        if (decorateCommands.contains(c[0])) System.out.println(decorateString);
        try {
            if (c[0].equals("h") || c[0].equals("help") || c[0].equals("?"))
                printUsage();
            else if (c[0].equals("q"))
                System.exit(0);
            else if (c[0].equals("w"))
                showStock();
            else if (c[0].equals("t"))
                showTeam();
            else if (c[0].equals("c"))
                showCart();
            else if (c[0].equals("sa"))
                showAllHistory();
            else if (c[0].equals("s10"))
                show10History();
            else if (c[0].equals("p"))
                cartLogic.submitCurrentPurchase();
            else if (c[0].equals("r"))
                cartLogic.cancelCurrentPurchase();
            else if (c[0].equals("ac") && c.length == 3) {
                StockItem item = cartLogic.findStockItem(Long.parseLong(c[1]));
                if (item != null) {
                    cartLogic.addItem(item, Integer.parseInt(c[2]));
                } else {
                    throw new SalesSystemException("Item not found!");
                }
            } else if (c[0].equals("rc") && c.length == 2) {
                StockItem item = warehouseLogic.findStockItem(Long.parseLong(c[1]));
                if (item != null) {
                    cartLogic.removeItem(item.getId());
                } else {
                    throw new SalesSystemException("Item with that id does not exist!");
                }
            }else if (c[0].equals("ap") && c.length == 5) {
                StockItem item = new StockItem(Long.parseLong(c[1]),  c[3],  "",
                        Math.round(Double.parseDouble(c[4]) * 100) / 100.0, Integer.parseInt(c[2]));
                warehouseLogic.addProduct(item);
            }
            else if (c[0].equals("rp") && c.length == 2) {
                if (warehouseLogic.findStockItem(Long.parseLong(c[1])) == null){
                    throw new SalesSystemException("Item with that id does not exist!");
                } else {
                    warehouseLogic.removeStockItem(Long.parseLong(c[1]));
                }
            }else if (c[0].equals("s") && c.length == 3) {
                showBetweenHistory(c[1], c[2]);
            }else if (c[0].equals("sp") && c.length == 2) {
                if (historyLogic.findPurchaseItem(Long.parseLong(c[1])) == null){
                    throw new SalesSystemException("Purchase with that id does not exist!");
                } else {
                    showPurchase(c[1]);
                }
            }else {
                throw new SalesSystemException("Unknown command! Get help");
            }
        } catch (Exception e) {
            new UupsiWhoopsieTechnicalSpeaker().setE(e).naita();
        }
        if (decorateCommands.contains(c[0])) System.out.println(decorateString);
     }
}
